/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

MainView {
    id: root
    applicationName: "owncloud-bookmarks.mardy"
    automaticOrientation: true

    width: units.gu(40)
    height: units.gu(75)

    property bool initialized: false
    property var activeTransfer: null

    Timer {
        interval: 500; running: true; repeat: false
        onTriggered: root.initialized = true
    }

    Connections {
        target: root.activeTransfer
        onStateChanged: {
            console.log("StateChanged: " + root.activeTransfer.state)
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }
    }

    Connections {
        target: ContentHub

        onImportRequested: {
            root.activeTransfer = transfer
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }

        onShareRequested: {
            root.activeTransfer = transfer
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }
    }

    Accounts {
        id: accounts

        onAuthenticatedChanged: if (authenticated) {
            bookmarks.login(username, password)
        }
    }

    BookmarkModel {
        id: bookmarks
        host: accounts.host
    }

    PageStack {
        id: pageStack

        Component.onCompleted: pageStack.push(mainPage)

        Page {
            id: mainPage
            visible: false

            header: PageHeader {
                title: i18n.tr("ownCloud bookmarks")
                flickable: bookmarkView.visible ? bookmarkView : null
                trailingActionBar.actions: [
                    Action {
                        iconName: "contact"
                        onTriggered: accounts.choose()
                    }
                ]
            }

            ActivityIndicator {
                anchors.centerIn: parent
                visible: !initialized
                running: !initialized
            }

            BookmarkView {
                id: bookmarkView
                anchors.fill: parent
                model: bookmarks
                accounts: accounts
                visible: initialized && bookmarks.count > 0
            }

            ImportPrompt {
                id: importPrompt
                visible: initialized && bookmarks.count === 0
                onImportRequested: pageStack.push(Qt.resolvedUrl("ImportPage.qml"))
            }
        }
    }

    function shareItems(items) {
        for (var i = 0; i < items.length; i++) {
            var url = items[i].url
            console.log("Adding URL: " + url)
            // uploadModel.addPhoto(url)
        }
    }
}
